#include "session.hpp"

Session::Session(boost::asio::ip::tcp::socket socket) : NetworkAgent(std::move(socket))
{
	start();
}

void Session::start()
{
	do_read_header();
}

void Session::impl_deliver(const Message& msg)
{
	writeMessage(msg);
}

void Session::disconnect()
{
	triggerDisconnect();
}

void Session::process(const Message& message)
{
	triggerNewMessage(message);
}
