#include "clientpool.hpp"

ClientPool::ClientPool(iProtocoleDecoder& decoder) : m_decoder(decoder)
{
}

void ClientPool::leave(iParticipant* participant)
{
	auto it = std::find_if(m_participants.begin(), m_participants.end(),
						   [participant](const participant_up& p)
	{ return p.get() == participant; });
	assert(it != m_participants.end());
	m_participants.erase(it);
}

void ClientPool::onNewMessage(iParticipant& participant, const Message& msg)
{
	m_decoder.parse(participant, msg);
}

void ClientPool::addNewSession(participant_up p)
{
	p->onDisconnect(std::bind(&ClientPool::leave, this, std::placeholders::_1));
	p->onNewMessage(
		std::bind(&ClientPool::onNewMessage, this, std::placeholders::_1, std::placeholders::_2));
	m_participants.insert(std::move(p));
}

void ClientPool::sendTextToAll(const std::string& text)
{
	for(auto& participant : m_participants)
		participant->deliver(text);
}
