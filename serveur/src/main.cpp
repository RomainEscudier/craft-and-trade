#include <iostream>

#include "serveur.hpp"

#include <list>

int main(int argc, char* argv[])
{
	try
	{
		if(argc != 2)
		{
			std::cerr << "Usage: chat_server <port>\n";
			return 1;
		}

		boost::asio::io_service io_service;
		boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::tcp::v4(), std::atoi(argv[1]));
		Serveur serveur(io_service, endpoint);

		io_service.run();
	}
	catch(std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}
	return 0;
}