#include "protocoledecoder.hpp"
#include "iparticipant.hpp"

#include <message.hpp>

#include <cassert>

#include <boost/property_tree/xml_parser.hpp>

#include <iostream>

Message ProtocoleDecoder::creerReponseActionNonPriseEnCompte()
{
	return Message("<reponse><name>erreur</name><param><type>action non prise "
				   "en compte</type></param></reponse>");
}

Message ProtocoleDecoder::creerReponseActionInvalide()
{
	return {"<reponse><name>erreur</name><param><type>action "
			"invalide</type></param></reponse>"};
}

Message ProtocoleDecoder::creerReponseLoginValide()
{
	return Message("<reponse><name>ack</name><param><type>login "
				   "valide</type></param></reponse>");
}

Message ProtocoleDecoder::creerReponseLoginInvalide()
{
	return Message("<reponse><name>erreur</name><param><type>login "
				   "invalide</type></param></reponse>");
}

void ProtocoleDecoder::impl_parse(iParticipant& participant, const Message& message)
{
	try
	{
		std::istringstream stream(message.string());
		property_tree_t tree;
		boost::property_tree::xml_parser::read_xml(stream, tree);
		participant.deliver(parseAction(tree));
	}
	catch(std::runtime_error& err)
	{
		participant.deliver(creerReponseActionInvalide());
	}
}

Message ProtocoleDecoder::parseAction(const property_tree_t& tree) const
{
	auto actionName = tree.get<std::string>("action.name");
	if(actionName == "login")
		return parseLogin(tree);

	return creerReponseActionNonPriseEnCompte();
}

Message ProtocoleDecoder::parseLogin(const property_tree_t& tree) const
{
	const auto login = tree.get<std::string>("action.param.login");
	const auto pswd = tree.get<std::string>("action.param.password");
	if(loginValide(login, pswd))
		return creerReponseLoginValide();
	else
		return creerReponseLoginInvalide();
}

bool ProtocoleDecoder::loginValide(const std::string& login, const std::string& pswd) const
{
	return login == "test" && pswd == "test";
}
