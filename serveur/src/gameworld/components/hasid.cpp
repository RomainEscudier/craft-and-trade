#include "gameworld/components/hasid.hpp"

BEGIN_NAMESPACE_GAMEWORLD

BEGIN_NAMESPACE_COMPONENTS

HasId::HasId(id_t id_) : m_id(id_)
{
}

auto HasId::id() const -> id_t
{
	return m_id;
}

END_NAMESPACE_COMPONENTS

END_NAMESPACE_GAMEWORLD
