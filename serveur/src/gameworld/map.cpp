#include "gameworld/map.hpp"
#include "gameworld/map_impl.hpp"

BEGIN_NAMESPACE_GAMEWORLD

Map::Map(id_t id_) : utils::pimpl<MapImpl>(id_)
{
}

Map::~Map()
{
}

auto Map::id() const -> id_t
{
	return get().id();
}

END_NAMESPACE_GAMEWORLD
