#include "gameworld/object.hpp"
#include "gameworld/object_impl.hpp"

BEGIN_NAMESPACE_GAMEWORLD

Object::Object(id_t id_) : utils::pimpl<ObjectImpl>(id_)
{
}

Object::~Object()
{
}

auto Object::id() const -> id_t
{
	return get().id();
}

END_NAMESPACE_GAMEWORLD
