#include "gameworld/player.hpp"
#include "gameworld/player_impl.hpp"

BEGIN_NAMESPACE_GAMEWORLD

Player::Player(id_t id_) : utils::pimpl<PlayerImpl>(id_)
{
}

Player::~Player()
{
}

auto Player::id() const -> id_t
{
	return get().id();
}

END_NAMESPACE_GAMEWORLD
