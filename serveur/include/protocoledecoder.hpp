#ifndef __PROTOCOLEDECODER_HPP__
#define __PROTOCOLEDECODER_HPP__

#include "iprotocoledecoder.hpp"

#include <string>

#include <boost/property_tree/ptree.hpp>

class ProtocoleDecoder : public iProtocoleDecoder
{
public:
	using property_tree_t = boost::property_tree::ptree;

	ProtocoleDecoder() = default;

	virtual ~ProtocoleDecoder() = default;

	static Message creerReponseActionNonPriseEnCompte();
	static Message creerReponseActionInvalide();
	static Message creerReponseLoginValide();
	static Message creerReponseLoginInvalide();

private:
	virtual void impl_parse(iParticipant& participant, const Message& message) override;

	Message parseAction(const property_tree_t& tree) const;

	Message parseLogin(const property_tree_t& tree) const;

	bool loginValide(const std::string& login, const std::string& pswd) const;
};

#endif
