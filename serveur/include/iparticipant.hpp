#ifndef __IPARTICIPANT_HPP__
#define __IPARTICIPANT_HPP__

#include <utils/noncopiable.hpp>

#include <memory>

#include <boost/signals2.hpp>

class Message;

class iParticipant : private utils::noncopiable
{
public:
	using disconnect_signal_t = boost::signals2::signal<void(iParticipant*)>;
	using new_message_signal_t = boost::signals2::signal<void(iParticipant&, const Message& msg)>;

	virtual ~iParticipant() = default;

	void deliver(const Message& msg);

	boost::signals2::connection onDisconnect(const disconnect_signal_t::slot_type& slot);

	boost::signals2::connection onNewMessage(const new_message_signal_t::slot_type& slot);

protected:
	iParticipant() = default;

	void triggerDisconnect();

	void triggerNewMessage(const Message& msg);

private:
	virtual void impl_deliver(const Message& msg) = 0;

	disconnect_signal_t m_disconnect;
	new_message_signal_t m_newMessage;
};

#endif
