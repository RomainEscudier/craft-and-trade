#ifndef __SERVEUR_HPP__
#define __SERVEUR_HPP__

#include "clientpool.hpp"
#include "protocoledecoder.hpp"

class Serveur
{
public:
	Serveur(boost::asio::io_service& io_service, const boost::asio::ip::tcp::endpoint& endpoint);

private:
	void do_accept();

	boost::asio::ip::tcp::acceptor m_acceptor;
	boost::asio::ip::tcp::socket m_socket;

	ProtocoleDecoder m_protocole_decoder;
	ClientPool m_pool;
};

#endif