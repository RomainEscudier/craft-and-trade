#ifndef __IPROTOCOLE_DECODER_HPP__
#define __IPROTOCOLE_DECODER_HPP__

#include <utils/noncopiable.hpp>

class Message;
class iParticipant;

class iProtocoleDecoder : private utils::noncopiable
{
public:
	virtual ~iProtocoleDecoder() = default;

	void parse(iParticipant& participant, const Message& message);

protected:
	iProtocoleDecoder() = default;

private:
	virtual void impl_parse(iParticipant& participant, const Message& message) = 0;
};

#endif
