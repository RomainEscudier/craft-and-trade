#ifndef __MAP_HPP__
#define __MAP_HPP__

#include <utils/pimpl.hpp>

#include "namespace_gameworld.hpp"

#include "types.hpp"

BEGIN_NAMESPACE_GAMEWORLD

class MapImpl;

class Map : private utils::pimpl<MapImpl>
{
public:
	Map(id_t id_);

	~Map();

	id_t id() const;
};

END_NAMESPACE_GAMEWORLD

#endif
