/*
 * HasId.hpp
 *
 *  Created on: 7 sept. 2014
 *      Author: epsonik
 */

#ifndef __HASID_HPP__
#define __HASID_HPP__

#include <cstdint>

#include "namespace_components.hpp"

#include "gameworld/namespace_gameworld.hpp"
#include "gameworld/types.hpp"

BEGIN_NAMESPACE_GAMEWORLD

BEGIN_NAMESPACE_COMPONENTS

class HasId
{
public:
	using id_t = GameWorld::id_t;

	HasId(id_t id_);
	~HasId() = default;

	id_t id() const;

private:
	id_t m_id;
};

END_NAMESPACE_COMPONENTS

END_NAMESPACE_GAMEWORLD

#endif /* HASID_HPP_ */
