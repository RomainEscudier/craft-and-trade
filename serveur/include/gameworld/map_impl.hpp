#ifndef __MAPIMPL_HPP__
#define __MAPIMPL_HPP__

#include <vector>

#include "namespace_gameworld.hpp"
#include "player.hpp"
#include "object.hpp"

#include "components/hasid.hpp"

BEGIN_NAMESPACE_GAMEWORLD

class MapImpl : private Components::HasId
{
public:
	using Components::HasId::id_t;

	MapImpl(id_t id_);

	using Components::HasId::id;

private:
	using player_container_t = std::vector<Player>;
	player_container_t m_players;

	using object_container_t = std::vector<Object>;
	object_container_t m_objects;
};

END_NAMESPACE_GAMEWORLD

#endif
