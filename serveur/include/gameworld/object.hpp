#ifndef __OBJECT_HPP__
#define __OBJECT_HPP__

#include <utils/pimpl.hpp>

#include "namespace_gameworld.hpp"

#include "types.hpp"

BEGIN_NAMESPACE_GAMEWORLD

class ObjectImpl;

class Object : private utils::pimpl<ObjectImpl>
{
public:
	Object(id_t id_);

	~Object();

	id_t id() const;
};

END_NAMESPACE_GAMEWORLD

#endif
