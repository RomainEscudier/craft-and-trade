#ifndef __OBJECTIMPL_HPP__
#define __OBJECTIMPL_HPP__

#include "namespace_gameworld.hpp"
#include "components/hasid.hpp"

BEGIN_NAMESPACE_GAMEWORLD

class ObjectImpl : private Components::HasId
{
public:
	using Components::HasId::id_t;

	ObjectImpl(id_t id_);

	using Components::HasId::id;
};

END_NAMESPACE_GAMEWORLD

#endif
