#ifndef __PLAYERIMPL_HPP__
#define __PLAYERIMPL_HPP__

#include "namespace_gameworld.hpp"
#include "components/hasid.hpp"

BEGIN_NAMESPACE_GAMEWORLD

class PlayerImpl : private Components::HasId
{
public:
	using Components::HasId::id_t;

	PlayerImpl(id_t id_);

	using Components::HasId::id;
};

END_NAMESPACE_GAMEWORLD

#endif
