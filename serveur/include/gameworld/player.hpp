#ifndef __PLAYER_HPP__
#define __PLAYER_HPP__

#include <utils/pimpl.hpp>

#include "namespace_gameworld.hpp"

#include "types.hpp"

BEGIN_NAMESPACE_GAMEWORLD

class PlayerImpl;

class Player : private utils::pimpl<PlayerImpl>
{
public:
	Player(id_t id_);
	~Player();

	id_t id() const;
};

END_NAMESPACE_GAMEWORLD

#endif
