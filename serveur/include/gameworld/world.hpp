#ifndef __WORLD_HPP__
#define __WORLD_HPP__

#include <utils/pimpl.hpp>

#include "namespace_gameworld.hpp"

BEGIN_NAMESPACE_GAMEWORLD

class WorldImp;

class World : private utils::pimpl<WorldImp>
{
	World() = default;
};

END_NAMESPACE_GAMEWORLD

#endif
