#ifndef __WORLDIMPL_HPP__
#define __WORLDIMPL_HPP__

#include <vector>

#include "map.hpp"

BEGIN_NAMESPACE_GAMEWORLD

class WorldImpl
{
	WorldImpl() = default;

private:
	using map_container_t = std::vector<Map>;
	map_container_t m_maps;
};

END_NAMESPACE_GAMEWORLD

#endif
