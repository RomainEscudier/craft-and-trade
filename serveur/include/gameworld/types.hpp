#ifndef __NAMESPACE_TYPES_HPP__
#define __NAMESPACE_TYPES_HPP__

#include "namespace_gameworld.hpp"

BEGIN_NAMESPACE_GAMEWORLD

using id_t = uint32_t;

END_NAMESPACE_GAMEWORLD

#endif
