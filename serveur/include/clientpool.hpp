#ifndef __CLIENTPOOL_HPP__
#define __CLIENTPOOL_HPP__

#include "iparticipant.hpp"
#include "iprotocoledecoder.hpp"

#include <message.hpp>
#include <utils/make_unique.hpp>

#include <boost/asio.hpp>

#include <set>

class ClientPool
{
public:
	using participant_up = std::unique_ptr<iParticipant>;

	ClientPool(iProtocoleDecoder& decoder);

	void leave(iParticipant* participant);

	void onNewMessage(iParticipant& participant, const Message& msg);

	template <class SESSION, class... Args> void addNewSession(Args&&... args)
	{
		auto p = utils::make_unique<SESSION>(std::forward<Args>(args)...);
		addNewSession(std::move(p));
	}

	void addNewSession(participant_up p);

	void sendTextToAll(const std::string& text);

private:
	std::set<participant_up> m_participants;

	iProtocoleDecoder& m_decoder;
};

#endif
