#include <gmock/gmock.h>

#include <iparticipant.hpp>

#include "basereseauavecvraidecoder.hpp"

using ::testing::_;
using ::testing::Eq;

class SessionBasique : public iParticipant
{
	void impl_deliver(const Message& /*msg*/) override
	{
	}

public:
	void process(const Message& message)
	{
		triggerNewMessage(message);
	}
};

struct MockProtocoleDecoder : public iProtocoleDecoder
{
	MOCK_METHOD2(impl_parse, void(iParticipant&, const Message&));
};

struct TestBaseReseau : public ::testing::Test
{
	virtual ~TestBaseReseau() = default;

	MockProtocoleDecoder decoder = {};
	ClientPool pool = {decoder};
};

TEST_F(TestBaseReseau, SessionRecoitMessageEtAppelleProtocoleDecoder)
{
	auto session_ptr = utils::make_unique<SessionBasique>();
	auto& session = *session_ptr;

	EXPECT_CALL(decoder, impl_parse(_, _)).Times(1);

	pool.addNewSession(std::move(session_ptr));
	session.process(Message(""));
}

struct TestBaseReseauAvecVraiDecoder : public BaseReseauAvecVraiDecoderAvecUneSession
{
	static Message creerMessageActionNonPriseEnCompte()
	{
		return Message("<action><name>action inconnue</name></action>");
	}

	static Message creerMessageInvalide()
	{
		return {"<action><name>ok"};
	}
};

TEST_F(TestBaseReseauAvecVraiDecoder, TestRequeteNonPriseEnCompte)
{
	EXPECT_CALL(m_session, impl_deliver(Eq(ProtocoleDecoder::creerReponseActionNonPriseEnCompte())))
		.Times(1);
	m_session.process(creerMessageActionNonPriseEnCompte());
}

TEST_F(TestBaseReseauAvecVraiDecoder, TestRequeteInvalide)
{
	EXPECT_CALL(m_session, impl_deliver(Eq(ProtocoleDecoder::creerReponseActionInvalide())))
		.Times(1);
	m_session.process(creerMessageInvalide());
}
