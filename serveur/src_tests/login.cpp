/*
 * login.cpp
 *
 *  Created on: 6 juin 2014
 *      Author: epso
 */

#include "basereseauavecvraidecoder.hpp"

using ::testing::_;
using ::testing::Eq;

struct TestLogin : public BaseReseauAvecVraiDecoderAvecUneSession
{
	virtual ~TestLogin() = default;

	static Message creerMessageLoginInvalide()
	{
		return Message(std::string("<action><name>login</name><param><login>mauvais_login</login>")
					   + "<password>mauvais_mdp</password></param></action>");
	}

	static Message creerMessageLoginValide()
	{
		return Message(std::string("<action><name>login</name><param><login>test</login>")
					   + "<password>test</password></param></action>");
	}
};

TEST_F(TestLogin, TestLoginInvalide)
{
	EXPECT_CALL(m_session, impl_deliver(Eq(ProtocoleDecoder::creerReponseLoginInvalide())))
		.Times(1);
	m_session.process(creerMessageLoginInvalide());
}

TEST_F(TestLogin, TestLoginValide)
{
	EXPECT_CALL(m_session, impl_deliver(Eq(ProtocoleDecoder::creerReponseLoginValide()))).Times(1);
	m_session.process(creerMessageLoginValide());
}
