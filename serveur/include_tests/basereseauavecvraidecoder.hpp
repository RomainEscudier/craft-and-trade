#ifndef __BASERESEAUAVECVRAIDECODER_HPP__
#define __BASERESEAUAVECVRAIDECODER_HPP__

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <clientpool.hpp>
#include <protocoledecoder.hpp>

struct MockSession : public iParticipant
{
	MOCK_METHOD1(impl_deliver, void(const Message&));

	void process(const Message& message)
	{
		triggerNewMessage(message);
	}
};

struct BaseReseauAvecVraiDecoder : public ::testing::Test
{
	virtual ~BaseReseauAvecVraiDecoder() = default;

	ProtocoleDecoder decoder = {};
	ClientPool pool = {decoder};
};

struct BaseReseauAvecVraiDecoderAvecUneSession : public BaseReseauAvecVraiDecoder
{
	virtual ~BaseReseauAvecVraiDecoderAvecUneSession() = default;

	BaseReseauAvecVraiDecoderAvecUneSession()
		: BaseReseauAvecVraiDecoder()
		, m_session(ajouteSession())
	{
	}

	MockSession& ajouteSession()
	{
		auto session_ptr = utils::make_unique<MockSession>();
		auto& session = *session_ptr;
		pool.addNewSession(std::move(session_ptr));
		return session;
	}

	MockSession& m_session;
};

#endif