cmake_minimum_required(VERSION 2.8)

message(STATUS "TARGET_NAME: " ${TARGET_NAME})
message(STATUS "path: " ${CMAKE_CURRENT_SOURCE_DIR})

set(BIN_DIR ${CMAKE_CURRENT_SOURCE_DIR}/bin/${TARGET_NAME})
message(STATUS "bin dir: " ${BIN_DIR})

set(LIBRARY_OUTPUT_PATH ${BIN_DIR})
set(EXECUTABLE_OUTPUT_PATH ${BIN_DIR})

add_subdirectory(commun)
add_subdirectory(serveur)
add_subdirectory(test)
