#ifndef __MESSAGE_HPP__
#define __MESSAGE_HPP__

#include <array>
#include <deque>
#include <string>

class Message
{
public:
	constexpr static std::size_t header_length = 4;
	constexpr static std::size_t max_body_length = 512;

	Message() = default;
	Message(const std::string& str);
	Message(const Message&) = default; // ne peut pas être delete sans
	// generalized lambda capture (ou trick)
	// :(
	Message(Message&&) = default;

	std::size_t size() const;

	std::string header() const;

	char* data();
	const char* data() const;

	char* body();
	const char* body() const;

	std::size_t body_length() const;

	bool decode_header();

	void encode_header();

	std::string string() const;

	bool operator==(const Message& m) const;

private:
	std::array<char, header_length + max_body_length> m_data;
	std::size_t m_body_length = 0;
};

using message_queue_t = std::deque<Message>;

#endif
