#ifndef __NETWORKAGENT_HPP__
#define __NETWORKAGENT_HPP__

#include "message.hpp"

#include <boost/asio.hpp>

class Message;

class NetworkAgent
{
protected:
	virtual ~NetworkAgent() = default;

	NetworkAgent(boost::asio::ip::tcp::socket socket);

	NetworkAgent(boost::asio::io_service& io_service);

	void writeMessage(const Message& msg);

	void do_write();

	void do_read_header();

	void do_read_body();

	boost::asio::ip::tcp::socket m_socket;

private:
	Message m_read_msg;
	message_queue_t m_write_msgs;

	virtual void disconnect() = 0;

	virtual void process(const Message& message) = 0;

	void actuallyWrite(boost::system::error_code ec, std::size_t length);

	void actuallyReadHeader(boost::system::error_code ec, std::size_t length);

	void actuallyReadBody(boost::system::error_code ec, std::size_t length);
};

#endif
