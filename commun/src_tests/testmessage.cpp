#include <gtest/gtest.h>

#include <message.hpp>

#include <cstring>

struct TestMessage : public ::testing::Test
{
	std::string buildHeader(std::size_t size)
	{
		auto h = std::to_string(size);
		if(h.size() < Message::header_length)
		{
			h.insert(0, Message::header_length - h.size(), ' ');
		}
		return h;
	}
};

TEST_F(TestMessage, CreationMessageVideParDefaut)
{
	Message m;
	ASSERT_EQ(0, m.body_length());
	ASSERT_EQ(Message::header_length, m.size());
}

TEST_F(TestMessage, TailleValideApresCreationDepuisString)
{
	const std::string str = "Juste un test pour voir";
	Message m(str);
	ASSERT_EQ(str.size(), m.body_length());
	ASSERT_EQ(str, m.string());

	// vérification du header
	auto h = buildHeader(str.size());
	ASSERT_EQ(h, m.header());
}

TEST_F(TestMessage, TailleCorpsValideApresDecodageEnTete)
{
	const std::size_t taille = 15;
	Message m;
	auto h = buildHeader(taille);
	std::memcpy(m.data(), h.data(), Message::header_length);
	m.decode_header();
	ASSERT_EQ(taille, m.body_length());
}