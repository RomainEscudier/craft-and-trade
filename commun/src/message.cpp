#include "message.hpp"

#include <cassert>
#include <cstring>

constexpr std::size_t Message::header_length;
constexpr std::size_t Message::max_body_length;

Message::Message(const std::string& str)
{
	assert(str.size() <= max_body_length);
	m_body_length = str.size();
	std::memcpy(body(), str.data(), m_body_length);
	encode_header();
}

std::size_t Message::size() const
{
	return header_length + m_body_length;
}

std::string Message::header() const
{
	return std::string(data(), header_length);
}

char* Message::data()
{
	return m_data.data();
}

const char* Message::data() const
{
	return m_data.data();
}

char* Message::body()
{
	return m_data.data() + header_length;
}

const char* Message::body() const
{
	return m_data.data() + header_length;
}

std::size_t Message::body_length() const
{
	return m_body_length;
}

bool Message::decode_header()
{
	char header[header_length + 1] = "";
	std::strncat(header, m_data.data(), header_length);
	m_body_length = std::atoi(header);
	if(m_body_length > max_body_length)
	{
		m_body_length = 0;
		return false;
	}
	return true;
}

void Message::encode_header()
{
	char header[header_length + 1] = "";
	std::sprintf(header, "%4d", (int)m_body_length);
	std::memcpy(m_data.data(), header, header_length);
}

std::string Message::string() const
{
	return std::string(body(), body_length());
}

bool Message::operator==(const Message& m) const
{
	return string() == m.string();
}
