#include "networkagent.hpp"

NetworkAgent::NetworkAgent(boost::asio::ip::tcp::socket socket) : m_socket(std::move(socket))
{
}

NetworkAgent::NetworkAgent(boost::asio::io_service& io_service) : m_socket(io_service)
{
}

void NetworkAgent::writeMessage(const Message& msg)
{
	bool write_in_progress = !m_write_msgs.empty();
	m_write_msgs.push_back(msg);
	if(!write_in_progress)
	{
		do_write();
	}
}

void NetworkAgent::do_write()
{
	boost::asio::async_write(
		m_socket, boost::asio::buffer(m_write_msgs.front().data(), m_write_msgs.front().size()),
		std::bind(&NetworkAgent::actuallyWrite, this, std::placeholders::_1,
				  std::placeholders::_2));
}

void NetworkAgent::do_read_header()
{
	boost::asio::async_read(m_socket,
							boost::asio::buffer(m_read_msg.data(), Message::header_length),
							std::bind(&NetworkAgent::actuallyReadHeader, this,
									  std::placeholders::_1, std::placeholders::_2));
}

void NetworkAgent::do_read_body()
{
	boost::asio::async_read(m_socket,
							boost::asio::buffer(m_read_msg.body(), m_read_msg.body_length()),
							std::bind(&NetworkAgent::actuallyReadBody, this, std::placeholders::_1,
									  std::placeholders::_2));
}

void NetworkAgent::actuallyWrite(boost::system::error_code ec, std::size_t /*length*/)
{
	if(!ec)
	{
		m_write_msgs.pop_front();
		if(!m_write_msgs.empty())
			do_write();
	}
	else
	{
		disconnect();
	}
}

void NetworkAgent::actuallyReadHeader(boost::system::error_code ec, std::size_t /*length*/)
{
	if(!ec && m_read_msg.decode_header())
	{
		do_read_body();
	}
	else
	{
		disconnect();
	}
}

void NetworkAgent::actuallyReadBody(boost::system::error_code ec, std::size_t /*length*/)
{
	if(!ec)
	{
		process(m_read_msg);
		do_read_header();
	}
	else
	{
		disconnect();
	}
}
