function lanceTest
{
#	echo $1
	compteurTotal=`expr $compteurTotal + 1`
	cp $1/* $tmpDir
	$exeClient --test $tmpDir/commands.txt > $tmpDir/result.txt
#	cat $tmpDir/result.txt
	diff $tmpDir/result.txt $tmpDir/expected.txt 2>/dev/null > /dev/null
	if [ $? -eq 0 ] 
	then 
		echo -n .
		compteurReussi=`expr $compteurReussi + 1`
	else
		echo -n F
		compteurErreurs=`expr $compteurErreurs + 1`
	fi
}


rootDir=`pwd`
testName=`basename $rootDir`

port=8686
exeClient=$rootDir/../../../bin/gcc/fake-client
exeServer=$rootDir/../../../bin/gcc/serveur

#création du dossier temporaire
tmpDir=$rootDir/tmp
mkdir $tmpDir

#lancement du serveur
$exeServer $port &
pidServer=$!
#echo $pidServer

compteurTotal=0
compteurReussi=0
compteurErreurs=0

for i in $testName*
do
	lanceTest $i
done

echo 
echo "$compteurTotal tests lancés: $compteurReussi réussis, $compteurErreurs erreurs"

rm -rf $tmpDir

kill $pidServer
