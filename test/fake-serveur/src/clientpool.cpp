#include "clientpool.hpp"
#include "session.hpp"

#include <utils/make_unique.hpp>

void ClientPool::leave(iParticipant* participant)
{
	// m_participants.erase(participant);
	auto it = std::find_if(m_participants.begin(), m_participants.end(),
						   [participant](const participant_up& p)
	{ return p.get() == participant; });
	assert(it != m_participants.end());
	m_participants.erase(it);
}

void ClientPool::onNewMessage(const Message& msg)
{
	std::cout << "message reçu: " << msg.string() << std::endl;
	sendTextToAll(msg.string());
}

void ClientPool::addNewSession(boost::asio::ip::tcp::socket socket)
{
	auto p = utils::make_unique<Session>(std::move(socket));
	p->onDisconnect(std::bind(&ClientPool::leave, this, std::placeholders::_1));
	p->onNewMessage(std::bind(&ClientPool::onNewMessage, this, std::placeholders::_1));
	m_participants.insert(std::move(p));
}

void ClientPool::sendTextToAll(const std::string& text)
{
	for(auto& participant : m_participants)
		participant->deliver(text);
}
