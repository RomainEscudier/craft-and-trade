#include "serveur.hpp"

Serveur::Serveur(boost::asio::io_service& io_service,
				 const boost::asio::ip::tcp::endpoint& endpoint)
	: m_acceptor(io_service, endpoint)
	, m_socket(io_service)
{
	do_accept();
}

void Serveur::do_accept()
{
	m_acceptor.async_accept(m_socket, [this](boost::system::error_code ec)
	{
		if(!ec)
		{
			m_pool.addNewSession(std::move(m_socket));
		}
		do_accept();
	});
}
