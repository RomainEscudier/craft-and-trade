#include "iparticipant.hpp"

void iParticipant::deliver(const Message& msg)
{
	impl_deliver(msg);
}

boost::signals2::connection iParticipant::onDisconnect(const disconnect_signal_t::slot_type& slot)
{
	return m_disconnect.connect(slot);
}

boost::signals2::connection iParticipant::onNewMessage(const new_message_signal_t::slot_type& slot)
{
	return m_newMessage.connect(slot);
}

void iParticipant::triggerDisconnect()
{
	m_disconnect(this);
}

void iParticipant::triggerNewMessage(const Message& msg)
{
	m_newMessage(msg);
}