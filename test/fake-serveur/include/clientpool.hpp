#ifndef __CLIENTPOOL_HPP__
#define __CLIENTPOOL_HPP__

#include "iparticipant.hpp"

#include <message.hpp>

#include <boost/asio.hpp>

#include <set>

class ClientPool
{
public:
	ClientPool() = default;

	void leave(iParticipant* participant);

	void onNewMessage(const Message& msg);

	void addNewSession(boost::asio::ip::tcp::socket socket);

	void sendTextToAll(const std::string& text);

private:
	using participant_up = std::unique_ptr<iParticipant>;
	std::set<participant_up> m_participants;
};

#endif
