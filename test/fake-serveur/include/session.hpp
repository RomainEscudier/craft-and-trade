#ifndef __SESSION_HPP__
#define __SESSION_HPP__

#include "iparticipant.hpp"

#include <networkagent.hpp>

class Session : public iParticipant, private NetworkAgent
{
public:
	Session(boost::asio::ip::tcp::socket socket);

	virtual ~Session() = default;

	void start();

private:
	virtual void impl_deliver(const Message& msg) override;

	virtual void disconnect() override;

	virtual void process(const Message& message) override;
};

#endif
