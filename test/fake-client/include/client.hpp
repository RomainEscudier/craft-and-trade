#ifndef __CLIENT_HPP__
#define __CLIENT_HPP__

#include <networkagent.hpp>

#include <boost/signals2.hpp>

class Client : private NetworkAgent
{
public:
	using signal_new_answer_t = boost::signals2::signal<void(const std::string&)>;

	Client(boost::asio::io_service& io_service,
		   boost::asio::ip::tcp::resolver::iterator endpoint_iterator);

	void close();

	void write(const Message& message);

	signal_new_answer_t& sNewAnswer();

private:
	void connect(boost::asio::ip::tcp::resolver::iterator endpoint_iterator);

	virtual void disconnect() override;

	virtual void process(const Message& message) override;

	boost::asio::io_service& m_io_service;
	signal_new_answer_t m_sNewAnswer;
};

#endif
