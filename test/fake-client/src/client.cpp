#include "client.hpp"

Client::Client(boost::asio::io_service& io_service,
			   boost::asio::ip::tcp::resolver::iterator endpoint_iterator)
	: NetworkAgent(io_service)
	, m_io_service(io_service)
{
	connect(endpoint_iterator);
}

void Client::close()
{
	m_io_service.post([this]()
	{ m_socket.close(); });
}

void Client::write(const Message& message)
{
	m_io_service.post([this, message]()
	{ writeMessage(message); });
}

auto Client::sNewAnswer() -> signal_new_answer_t &
{
	return m_sNewAnswer;
}

void Client::connect(boost::asio::ip::tcp::resolver::iterator endpoint_iterator)
{
	boost::asio::async_connect(
		m_socket, endpoint_iterator,
		[this](boost::system::error_code ec, boost::asio::ip::tcp::resolver::iterator)
	{
			if(!ec)
			{
				do_read_header();
			}
		});
}

void Client::disconnect()
{
	m_socket.close();
	exit(1);
}

void Client::process(const Message& message)
{
	m_sNewAnswer(message.string());
}
