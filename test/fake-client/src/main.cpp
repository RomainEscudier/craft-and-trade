#include <fstream>
#include <iostream>
#include <thread>

#include "client.hpp"

bool estTest(const std::string& param);
void lanceServeur(const std::string& ip, const std::string& port, std::istream& stream);

int main(int argc, char* argv[])
{
	try
	{
		if(argc != 3)
		{
			std::cerr << "Usage: fake-client <host> <port> of fake-client "
						 "--test commandFile\n";
			return 1;
		}

		if(estTest(argv[1]))
		{
			std::string ip;
			std::string port;
			std::ifstream commandFile(argv[2]);
			assert(commandFile);
			std::getline(commandFile, ip);
			assert(commandFile);
			std::getline(commandFile, port);
			assert(commandFile);
			lanceServeur(ip, port, commandFile);
		}
		else
		{
			lanceServeur(argv[1], argv[2], std::cin);
		}
	}
	catch(std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}

bool estTest(const std::string& param)
{
	return param == "--test";
}

void lanceServeur(const std::string& ip, const std::string& port, std::istream& stream)
{
	boost::asio::io_service io_service;

	boost::asio::ip::tcp::resolver resolver(io_service);
	auto endpoint_iterator = resolver.resolve({ip, port});

	Client c(io_service, endpoint_iterator);

	std::thread t([&io_service]()
	{ io_service.run(); });

	std::string line;
	while(std::getline(stream, line))
	{
		boost::optional<std::string> reponse;
		c.sNewAnswer().connect([&reponse](const std::string& str)
		{ reponse = str; });
		c.write(line);
		while(true)
		{
			if(reponse)
			{
				std::cout << *reponse << std::endl;
				break;
			}

			std::this_thread::sleep_for(std::chrono::seconds(1));
		}
	}

	c.close();
	t.join();
}